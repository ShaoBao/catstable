//
//  HTTPTask.swift
//  CatsTable
//
//  Created by Gleb on 27/04/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation


typealias Parameters = [String: Any]
typealias HTTPHeaders = [String: String]

enum HTTPTask {
    case requestPlain
    case requestParameters(bodyParameters   : Parameters?,
                           urlParameters    : Parameters?)
    case requestParamteresAndHeaders(bodyParameters: Parameters?,
                                     urlParameters: Parameters,
                                     additionalHeaders: HTTPHeaders)
}
