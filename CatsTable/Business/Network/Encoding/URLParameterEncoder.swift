//
//  URLParameterEncoder.swift
//  CatsTable
//
//  Created by Gleb on 27/04/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation


protocol ParameterEncoder {
    func encode(urlRequest: inout URLRequest, with parameters: Parameters)
    throws
}

struct URLParameterEncoder: ParameterEncoder {
    func encode(urlRequest: inout URLRequest, with parameters: Parameters) throws {
        if let url = urlRequest.url {
            if var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false),
                !parameters.isEmpty {
                urlComponents.queryItems = [URLQueryItem]()
                
                for (key, value) in parameters {
                    let queryItem = URLQueryItem(name: key, value: "\(value)".addingPercentEncoding(withAllowedCharacters: .urlHostAllowed))
                    urlComponents.queryItems?.append(queryItem)
                }
                urlRequest.url = urlComponents.url
            }
            if urlRequest.value(forHTTPHeaderField: "Content-Type") == nil {
                urlRequest.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
            }
        } else {
            print("error")
        }
    }  
}
