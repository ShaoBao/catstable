//
//  ParameterEncoding.swift
//  CatsTable
//
//  Created by Gleb on 27/04/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation


enum ParameterEncoding {
    case urlEncoding
    
    
    func encode(urlRequest: inout URLRequest, bodyParameters: Parameters?, urlParameters: Parameters?) throws {
        do {
            switch self {
            case .urlEncoding:
                guard let urlParameters = urlParameters else { throw NetworkError.parametersNull}
                try URLParameterEncoder().encode(urlRequest: &urlRequest, with: urlParameters)
            }
        } catch {
            throw error
        }
    }
}

enum NetworkError: String, Error {
    case parametersNull = "Parameters are nil"
    case missingURL     = "Url is nil"
    case endodeFail     = "Encoding failed"
    case unknown        = "Unkown Error"
}
