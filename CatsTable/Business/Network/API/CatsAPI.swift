//
//  CatsAPI.swift
//  CatsTable
//
//  Created by Gleb on 27/04/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation



enum CatsAPI {
    case search(limit: Int, page: Int)
    case favourite(id: String)
}

extension CatsAPI: EndPointType {
    
    var baseURL: URL {
        guard let url = URL(string: "https://api.thecatapi.com") else { fatalError("baseURL couldnt been configured")}
        return url
    }
    
    var path: String {
        switch self {
        case .search:
            return "/v1/images/search"
        case .favourite( _):
            return "/v1/favourites"
        }
    }
    
    var task: HTTPTask {
        switch  self {
        case .search(let limit, let page):
            return .requestParameters(bodyParameters: nil, urlParameters: ["page":page, "limit": limit])
        case .favourite(let id):
            return .requestParamteresAndHeaders(bodyParameters: nil, urlParameters: ["": id], additionalHeaders: headers!)
        }
    }
    
    var httpMethod: String {
        return "GET"
    }
    
    var headers: HTTPHeaders? {
        var headers: [String: String] = [:]
        headers["x-api-key"] = "32434d5d-a7b2-461d-84f2-3c002e3062fe"
        return headers
    }
    
    
}
