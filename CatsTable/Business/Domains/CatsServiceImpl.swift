//
//  CatsServiceImpl.swift
//  CatsTable
//
//  Created by Gleb on 27/04/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation


class CatsServiceImpl {
    
    let networkRouter:Router<CatsAPI>?
    
    init() {
        self.networkRouter = Router<CatsAPI>()
    }
    
}
extension CatsServiceImpl: CatsService {
    func requestCatById(id: String,completion: @escaping (CatsModel?, String?) -> Void) {
        networkRouter?.request(.favourite(id: id), completion: { (data, response, error) in
        })
    }
    
    func requestCatsAPI(completion: @escaping ([CatsModel]?, Data?, String?) -> Void) {
        networkRouter?.request(.search(limit: 20, page: 2), completion: { (data, response, error) in

            if let response = response as? HTTPURLResponse {
                let result = self.handleNetworkResponse(response)

                let catJson = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                
                let json = try?  JSONSerialization.data(withJSONObject: catJson as Any, options: .prettyPrinted)
                
                switch result {
                case .success( _):
                    guard let responseData = data  else {
                        completion(nil, nil, NetworkError.missingURL.rawValue )
                        return
                    }
                    do {
                        let catsDAta = try JSONDecoder().decode([CatsModel].self, from: responseData)
                        completion(catsDAta, json!, nil)
                    } catch {
                        completion(nil, nil, NetworkError.parametersNull.rawValue)
                    }
                    
                default: break
                }
            }
        })
    }
    
    func handleNetworkResponse(_ response: HTTPURLResponse) -> Result<String, Error> {
        switch response.statusCode {
        case 200...299: return  .success("success")
    
        default: return .failure(NetworkError.parametersNull)
        }
    }
}
