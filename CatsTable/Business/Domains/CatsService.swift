//
//  CatsService.swift
//  CatsTable
//
//  Created by Gleb on 27/04/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation


protocol CatsService {
   func requestCatsAPI(completion: @escaping ([CatsModel]?, Data?, String?) -> Void )
   func requestCatById(id: String, completion: @escaping (CatsModel?,String?) -> Void  )
}
