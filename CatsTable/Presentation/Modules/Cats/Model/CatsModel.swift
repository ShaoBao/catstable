//
//  CatsModel.swift
//  CatsTable
//
//  Created by Gleb on 27/04/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation


struct CatsModel: Decodable {
        var id       : String?
        var url      : String?
        var height   : Int?
}
