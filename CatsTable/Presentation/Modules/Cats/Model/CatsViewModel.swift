//
//  CatsViewModel.swift
//  CatsTable
//
//  Created by Gleb on 27/04/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation


struct CatsViewModel {
    
    struct Cat {
        var id       : String?
        var url      : String?
        var height   : Int?
    }
    var cats: [Cat]?
    var catJSON: Data?
}
