//
//  CatsTableViewCell.swift
//  CatsTable
//
//  Created by Gleb on 27/04/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import UIKit

class CatsTableViewCell: UITableViewCell {
    
    var catImage: UIImageView!
    
    private var catLabel: UILabel!
    
    var imageWidthConstraint: NSLayoutConstraint!
    
    var catsTableCellLayout =  CatsTableCellLayout()

    func resizeImage(value: CGFloat, width: Int , height: Int) {
        catImage.contentMode = .scaleAspectFill
        let totalFreeSpaceForWidth = UIScreen.main.bounds.width - 48 - 34
        if totalFreeSpaceForWidth > value {
             self.catsTableCellLayout.widthAnchor?.constant = value
        }
        if value > totalFreeSpaceForWidth {
            self.catsTableCellLayout.widthAnchor?.constant = totalFreeSpaceForWidth
        }
        let width = Int(width)
        self.catLabel.text = "width \(width)\nheight\(height)"
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    private func setupViews() {
        setupCatLabel()
        setupCatImage()
        
    }
    
    private func setupCatImage() {
        catImage = UIImageView()
        catImage.image = UIImage(named: "placeHold")
        catImage.contentMode = .scaleAspectFit
        catImage.translatesAutoresizingMaskIntoConstraints = false
        catImage.layer.cornerRadius = 10
        catImage.layer.masksToBounds = true
        addSubview(catImage)
        catsTableCellLayout.initial(catImage, label: catLabel)
    }
    
    private func setupCatLabel() {
        catLabel = UILabel()
        catLabel.font = UIFont.systemFont(ofSize: 10)
        catLabel.textAlignment = .left
        catLabel.lineBreakMode = .byWordWrapping
        catLabel.numberOfLines = 0
        catLabel.translatesAutoresizingMaskIntoConstraints  = false
        addSubview(catLabel)
        catsTableCellLayout.initial(label: catLabel)
    }
    
}
