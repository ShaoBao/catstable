//
//  CatsTableDataSource.swift
//  CatsTable
//
//  Created by Gleb on 27/04/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation
import UIKit


class CatsTableDataSource: NSObject {
    
    var viewModel: CatsViewModel?
    var imageRecords: ImageRecord?
    var table: UITableView?
    var pendingQueue: PendingQueue?
    
    convenience init(table: UITableView, pendingQueue: PendingQueue) {
        self.init()
        self.table = table
        self.pendingQueue = pendingQueue
    }
    
    func downloadImage(imageRecord: ImageRecord, indexPath: IndexPath) {
        
        let downloader = ImageLoader(imageRecord: imageRecord)
        downloader.completionBlock = {
            if downloader.isCancelled {
                return
            }
            DispatchQueue.main.async {
                self.table?.reloadRows(at: [indexPath], with: .none)
            }
        }
        pendingQueue?.downloadingQueue.addOperation(downloader)
    }
}

extension CatsTableDataSource: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return imageRecords?.records.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CatsTableViewCell", for: indexPath) as? CatsTableViewCell
        cell?.selectionStyle = .none
        let currentImageRecord = imageRecords?.records[indexPath.row]
        cell!.catImage.image = currentImageRecord?.image
        switch  currentImageRecord?.state {
        case .new:
            downloadImage(imageRecord: currentImageRecord!, indexPath: indexPath)
        case.downloaded:
            cell?.resizeImage(value: currentImageRecord?.widthConstraint?.constant ?? 0, width: currentImageRecord?.imageWidth ?? 0, height: currentImageRecord?.imageHeight ?? 0)
        default:
            break
        }
        return cell!
    }
}

extension CatsTableDataSource: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 82
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var catData = ["catId": viewModel?.cats?[indexPath.row].id as? Any]
        catData["catImage"] = viewModel?.cats?[indexPath.row].url as AnyObject?

        let json = (try? JSONSerialization.jsonObject(with: viewModel!.catJSON!, options: .allowFragments) as? [[String: Any]])!
        
        
        for (i,v) in json.enumerated() {
            if i == indexPath.row {
                catData["catJSON"] = v as? AnyObject
            }
            
        }
        
      
        NotificationCenter.default.post(name: NSNotification.Name("CatDataPassing"), object: nil, userInfo: catData as [AnyHashable : Any])
    }
}
