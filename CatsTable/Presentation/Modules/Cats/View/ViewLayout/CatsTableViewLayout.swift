//
//  CatsTableViewLayout.swift
//  CatsTable
//
//  Created by Gleb on 27/04/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation
import UIKit


struct CatsTableViewLayout {
    
    func initial(_ view: UITableView) {
        guard let superview = view.superview else { return }
        view.topAnchor.constraint(equalTo: superview.topAnchor, constant: 0)
            .isActive = true
        view.leftAnchor.constraint(equalTo: superview.leftAnchor, constant: 0)
            .isActive = true
        view.rightAnchor.constraint(equalTo: superview.rightAnchor, constant: 0)
            .isActive = true
        view.bottomAnchor.constraint(equalTo: superview.bottomAnchor, constant: 0)
            .isActive = true
    }
}
