//
//  CatsTableCellLayout.swift
//  CatsTable
//
//  Created by Gleb on 27/04/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation
import UIKit


struct CatsTableCellLayout {
    
    var widthAnchor: NSLayoutConstraint?
    
    mutating func initial(_ image: UIImageView, label: UILabel) {
       guard  let superview = image.superview else { return }
        image.topAnchor.constraint(equalTo: superview.topAnchor, constant: 16)
            .isActive = true
        image.rightAnchor.constraint(equalTo: label.leftAnchor, constant: -16)
            .isActive = true
        image.heightAnchor.constraint(equalToConstant: 50)
            .isActive = true
        
        let width = image.widthAnchor.constraint(equalToConstant: 120)
        width.isActive = true
        widthAnchor = width
        
    }
    
    func initial(label: UILabel) {
        guard let superview = label.superview else { return }
        label.topAnchor.constraint(equalTo: superview.topAnchor, constant: 16)
            .isActive = true
        label.rightAnchor.constraint(equalTo: superview.rightAnchor, constant: -16)
            .isActive = true
        label.heightAnchor.constraint(equalToConstant: 50)
            .isActive = true
        label.widthAnchor.constraint(equalToConstant: 34)
            .isActive = true
    }
}
