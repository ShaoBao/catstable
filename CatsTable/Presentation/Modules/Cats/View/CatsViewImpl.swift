//
//  CatsViewImpl.swift
//  CatsTable
//
//  Created by Gleb on 27/04/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation
import  UIKit


class CatsViewImpl: UIView {
    
    var tableView: UITableView!
    
    var imageRecords: ImageRecord?
    private var pendingQueue: PendingQueue?
    
    private let catsTableViewLayout =  CatsTableViewLayout()
    private  var catsTableDataSource: CatsTableDataSource?
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        setupViews()
    }
    
    private var viewModel: CatsViewModel? {
        didSet {
            updateSubviews()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        self.tableView = UITableView()
        pendingQueue = PendingQueue()
        catsTableDataSource = CatsTableDataSource(table: tableView, pendingQueue: pendingQueue!)
        tableView.delegate = catsTableDataSource
        tableView.dataSource = catsTableDataSource
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.tableFooterView = UITableViewHeaderFooterView()
        tableView.register(CatsTableViewCell.self, forCellReuseIdentifier:"CatsTableViewCell")
        self.addSubview(tableView)
        catsTableViewLayout.initial(tableView)
    }
    
    private func updateSubviews() {
        guard let viewModel = viewModel else { return }
        
        catsTableDataSource?.viewModel = viewModel
        let imageRec = ImageRecord()
        self.imageRecords = ImageRecord()
        viewModel.cats?.forEach {
            let imageRecord = ImageRecord(url: $0.url ?? "")
            imageRec.records.append(imageRecord)
            self.imageRecords?.records.append(imageRecord)
        }
        catsTableDataSource?.imageRecords = imageRec
        catsTableDataSource?.table = self.tableView
        tableView.reloadData()
    }
}

extension CatsViewImpl: CatsView {
    func update(with viewModel: CatsViewModel) {
        self.viewModel = viewModel
    } 
}
