//
//  CatsRouter.swift
//  CatsTable
//
//  Created by Gleb on 27/04/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation
import UIKit


protocol CatsRouter: class {
    func routeToDetails(viewController: UIViewController)
}
