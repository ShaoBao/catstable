//
//  CatsRouterImpl.swift
//  CatsTable
//
//  Created by Gleb on 27/04/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation
import  UIKit


class CatsRouterImpl {
    weak var viewController: CatsViewControllerImpl?
    
    init(viewController: CatsViewControllerImpl) {
        self.viewController = viewController
        NotificationCenter.default.addObserver(self, selector: #selector(passCatsData), name: NSNotification.Name.init("CatDataPassing"), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("CatDataPassing"), object: nil)
    }
    
    @objc func passCatsData(notification: Notification) {
        guard let id = notification.userInfo?["catId"] as? String else { return }
        guard let imageURLString = notification.userInfo?["catImage"] as? String else {
        return }
        guard let catJSON = notification.userInfo?["catJSON"]  else { return }

        let catDetailViewController = CatDetailsAssembly.configureModule()
        catDetailViewController.interactor.getId(id: id, imageURLString: imageURLString, catJSON: catJSON as AnyObject)
        routeToDetails(viewController: catDetailViewController)
       
    }
    
}
extension CatsRouterImpl: CatsRouter {
    
    func routeToDetails(viewController: UIViewController) {
        self.viewController?.navigationController?.pushViewController(viewController, animated: true)
    }
}
