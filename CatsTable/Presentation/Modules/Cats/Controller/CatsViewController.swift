//
//  CatsViewController.swift
//  CatsTable
//
//  Created by Gleb on 27/04/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation


enum CatsViewControllerState {
    case result(CatsViewModel)
}

protocol CatsViewController: class {
    func displayState(result: CatsViewControllerState)
}
