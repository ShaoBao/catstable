//
//  ViewController.swift
//  CatsTable
//
//  Created by Gleb on 27/04/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import UIKit

class CatsViewControllerImpl: UIViewController {
    
    var interactor: CatsInteractor!
    
    private var catsView: CatsView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //MARK: Temp solution cause at this endpoint always comes different data
        let isEmpty =  self.interactor.checkData()
        if isEmpty {
            self.interactor.loadCatsNetworkData()
        }
    }
    
    init() {
        super.init(nibName: .none , bundle: .none)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        let catsView = CatsViewImpl()
        catsView.frame = view.bounds
        catsView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.catsView = catsView
        view.addSubview(catsView)
    }
}

extension CatsViewControllerImpl: CatsViewController {
    func displayState(result: CatsViewControllerState) {
        switch result {
        case .result(let viewModel):
            catsView.update(with: viewModel)
        }
    }
}

