//
//  CatsInteractor.swift
//  CatsTable
//
//  Created by Gleb on 27/04/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation


protocol CatsInteractor: class {
    func loadCatsNetworkData()
    func checkData() -> Bool
}
