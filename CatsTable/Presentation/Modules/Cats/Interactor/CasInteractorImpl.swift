//
//  CasInteractorImpl.swift
//  CatsTable
//
//  Created by Gleb on 27/04/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation



class CatsInteractorImpl {
    
    private let catsService: CatsService
    private let presenter: CatsPresenter
    weak var viewController: CatsViewControllerImpl?
    
    var catId: String?
    var catDataInEmpty:Bool?
    
    init(catsService: CatsService, presenter: CatsPresenter) {
        self.catsService = catsService
        self.presenter = presenter
    }
}

extension CatsInteractorImpl: CatsInteractor {
    func checkData() -> Bool {
        if catDataInEmpty ?? true {
            return true
        } else {
            return false
        }
    }
    
    func loadCatsNetworkData() {
        self.catsService.requestCatsAPI {[weak self] (cats, catJSON, error) in
            switch cats {
            case .some( _):
                var catsViewModel = CatsViewModel(cats: []).cats
               
                cats?.forEach {
                    var cat = CatsViewModel.Cat()
                    cat.height = $0.height
                    cat.id = $0.id
                    cat.url = $0.url
                    catsViewModel?.append(cat)
                }
                var catsModel = CatsViewModel(cats: catsViewModel)
                catsModel.catJSON = catJSON
                self?.presenter.presentCatAPI(value: catsModel)
                self?.catDataInEmpty = false
            case .none:
                self?.catDataInEmpty = true
                break
            }
        }
    }
}
