//
//  CatsAssemly.swift
//  CatsTable
//
//  Created by Gleb on 27/04/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation

class CatsAssembly {
     static func configureModule() -> CatsViewControllerImpl {
      let viewController = CatsViewControllerImpl()

      let catsService = CatsServiceImpl()
      let router = CatsRouterImpl(viewController: viewController)
      let presenter = CatsPresenterImpl(controller: viewController, router: router)
      let interactor = CatsInteractorImpl(catsService: catsService, presenter: presenter)

        viewController.interactor = interactor
        interactor.viewController = viewController
        presenter.viewController = viewController
        router.viewController = viewController
        
        return viewController
     }
}
