//
//  CatsPresenterImpl.swift
//  CatsTable
//
//  Created by Gleb on 27/04/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation


class CatsPresenterImpl {
    
    var viewController: CatsViewControllerImpl?
    
    private let router: CatsRouter
    
    init(controller: CatsViewControllerImpl, router: CatsRouter) {
        self.viewController = controller
        self.router = router
    }
}

extension CatsPresenterImpl: CatsPresenter {
    func presentCatAPI(value: CatsViewModel) {
        DispatchQueue.main.async {
             self.viewController?.displayState(result: .result(value))
        }
    }
    
    
}
