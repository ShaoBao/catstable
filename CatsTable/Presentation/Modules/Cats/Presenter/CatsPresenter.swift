//
//  CatsPresenter.swift
//  CatsTable
//
//  Created by Gleb on 27/04/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation


protocol CatsPresenter: class {
    func presentCatAPI(value: CatsViewModel)
}
