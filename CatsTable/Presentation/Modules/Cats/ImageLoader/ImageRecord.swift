//
//  ImageRecord.swift
//  CatsTable
//
//  Created by Gleb on 27/04/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation
import UIKit


enum ImageState {
    case new
    case downloaded
    case failed
}

var imageCache =  NSCache<NSString, UIImage>()

class ImageRecord {
    static var shared = ImageRecord()
    var url: URL?
    var state: ImageState = ImageState.new
    var image: UIImage    = UIImage(named: "placeHold")!
    var records: [ImageRecord] = []
    var widthConstraint: NSLayoutConstraint?
    var imageWidth: Int?
    var imageHeight: Int?
    var imageCache = NSCache<NSString, UIImage>()

    init() {}
    
   convenience init(url: String) {
        self.init()
        self.url = URL(string: url)!
    }
}


class ImageLoader: Operation {
    var imageRecord: ImageRecord

    init(imageRecord: ImageRecord) {
        self.imageRecord = imageRecord
    }

    override func main() {
        if isCancelled {
            return
        }
        
        if imageRecord.imageCache.object(forKey: (imageRecord.url?.absoluteString as NSString? ?? "")) != nil {
            return
        }

        guard let data = try? Data(contentsOf: imageRecord.url!) else {
            return
        }
        
        if !data.isEmpty {
            imageRecord.image = UIImage(data: data)!
            imageRecord.state = .downloaded
            imageRecord.imageCache.setObject(UIImage(data: data)!, forKey: (imageRecord.url?.absoluteString as NSString?) ?? "")

            if let imageSource = CGImageSourceCreateWithURL(imageRecord.url! as CFURL, nil) {
                if let imageProperties = CGImageSourceCopyPropertiesAtIndex(imageSource, 0, nil) as Dictionary? {
                    let pixelWidth = imageProperties[kCGImagePropertyPixelWidth] as! Int
                    let pixelHeight = imageProperties[kCGImagePropertyPixelHeight] as! Int
                    let myScaleVariable = UIScreen.main.scale
                    let widthInPoints =  CGFloat(pixelWidth) / myScaleVariable
                    self.imageRecord.imageWidth = pixelWidth
                    self.imageRecord.imageHeight = pixelHeight
                    self.imageRecord.widthConstraint = NSLayoutConstraint()
                    self.imageRecord.widthConstraint?.constant = widthInPoints
                }
            }
        } else {
            imageRecord.image = UIImage(named: "failed")!
            imageRecord.state = .failed
        }
    }
}

class  PendingQueue: Operation {
   lazy var downloadingQueue: OperationQueue =  {
        let queue = OperationQueue()
        queue.maxConcurrentOperationCount = 2
        queue.name = "ImageDownload"
        return queue
    }()
}
