//
//  CatDetailViewLayout.swift
//  CatsTable
//
//  Created by Gleb on 28/04/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation
import UIKit



class CatDetailViewLayout {
    
    func initial(_ image: UIImageView) {
      guard  let superview = image.superview else { return }
       
        
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            let topPadding = window?.safeAreaInsets.top
            image.topAnchor.constraint(equalTo: superview.topAnchor, constant: topPadding! + 60)
                   .isActive = true
        }
        
        image.leftAnchor.constraint(equalTo: superview.leftAnchor, constant: 16)
        .isActive = true
        image.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width - 32)
        .isActive = true
        image.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.width - 32)
        .isActive = true
    }
    
    
    func initial(_ label: UITextView, image: UIImageView) {
        guard let superview = label.superview else { return }
        label.topAnchor.constraint(equalTo: image.bottomAnchor, constant: 16)
            .isActive = true
        label.leftAnchor.constraint(equalTo: superview.leftAnchor, constant: 16)
        .isActive = true
        label.rightAnchor.constraint(equalTo: superview.rightAnchor, constant: -16)
        .isActive = true
        label.bottomAnchor.constraint(equalTo: superview.bottomAnchor, constant: -16)
        .isActive = true
        
    }
}
