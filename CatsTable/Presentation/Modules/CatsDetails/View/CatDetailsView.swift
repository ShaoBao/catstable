//
//  CatDetailsView.swift
//  CatsTable
//
//  Created by Gleb on 28/04/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation
import UIKit

protocol CatDeTailsView: class {
    func updateViewModel(model: CatsViewModel.Cat)
    func updateImageAndData(image: Data, catJSON: AnyObject)
}
