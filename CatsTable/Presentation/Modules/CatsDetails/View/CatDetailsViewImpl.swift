//
//  CatDetailsViewImpl.swift
//  CatsTable
//
//  Created by Gleb on 28/04/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation
import UIKit


class CatDetailsViewImpl: UIView {

    var catDetailViewLayout = CatDetailViewLayout()
    
    var mainImage: UIImageView?
    var descriptionLabel: UITextView?
    

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        mainImage = UIImageView()
        mainImage?.contentMode = .scaleAspectFill
        mainImage?.translatesAutoresizingMaskIntoConstraints = false
        mainImage?.layer.masksToBounds = true
        self.addSubview(mainImage!)
        catDetailViewLayout.initial(mainImage!)
        
        descriptionLabel = UITextView()
        descriptionLabel?.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(descriptionLabel!)

        catDetailViewLayout.initial(descriptionLabel!, image: mainImage!)
    }
}

extension CatDetailsViewImpl: CatDeTailsView {
    func updateImageAndData(image: Data, catJSON: AnyObject) {
        let image = UIImage(data: image)
        self.mainImage?.image = image

        let catData = catJSON as? [String: Any]
        let json = try? JSONSerialization.data(withJSONObject: catData as Any, options: .prettyPrinted)
        
        let jsonString = String(data: json!, encoding: .utf8)
        self.descriptionLabel?.text = jsonString
    }
    
    func updateViewModel(model: CatsViewModel.Cat) {
        
    }
}
