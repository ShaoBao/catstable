//
//  CatDetailRouterImpl.swift
//  CatsTable
//
//  Created by Gleb on 28/04/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation


class CatDetailRouterImpl {
    
    var viewController: CatsDetailViewControllerImpl
    
    init(viewController: CatsDetailViewControllerImpl) {
        self.viewController = viewController
    }
}

extension CatDetailRouterImpl: CatDetailRouter {
    
}
