//
//  CatDetailsAssembly.swift
//  CatsTable
//
//  Created by Gleb on 28/04/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation

class CatDetailsAssembly {
    static func configureModule() -> CatsDetailViewControllerImpl {
        let viewController = CatsDetailViewControllerImpl()
        
        let catsService = CatsServiceImpl()
        let router = CatDetailRouterImpl(viewController: viewController)
        let presenter = CatDetailsPresenterImpl(viewController: viewController, router: router)
        let interactor = CatDetailInteractorImpl(catService: catsService, presenter: presenter)
        
        viewController.interactor = interactor
        interactor.viewController = viewController
        presenter.viewController = viewController
        router.viewController = viewController
        
        return viewController
    }
}
