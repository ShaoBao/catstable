//
//  CatsDetailViewControllerImpl.swift
//  CatsTable
//
//  Created by Gleb on 28/04/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation
import  UIKit


class CatsDetailViewControllerImpl: UIViewController {
    
    private var catDetailsView: CatDeTailsView!
    
    var interactor: CatDetailInteractorImpl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.interactor.getCatById()
    }
    
    init() {
        super.init(nibName: .none, bundle: .none)
        self.view.backgroundColor = .white
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        let catDetailsV = CatDetailsViewImpl()
        catDetailsV.frame = self.view.bounds
        catDetailsV.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.catDetailsView = catDetailsV
        view.addSubview(catDetailsV)
    }
}
extension CatsDetailViewControllerImpl: CatDetailsViewController {
    
    func displayImageAndData(image: Data, catJSON: AnyObject) {
         catDetailsView.updateImageAndData(image: image, catJSON: catJSON)
    }
    func displayState(result: CatsDisplayState) {
        
    }
}
