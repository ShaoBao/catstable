//
//  CatDetailsViewController.swift
//  CatsTable
//
//  Created by Gleb on 28/04/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation

enum CatsDisplayState {
    case result(CatsViewModel.Cat)
}

protocol CatDetailsViewController: class {
    func displayState(result: CatsDisplayState)
    func displayImageAndData(image: Data, catJSON: AnyObject)
}
