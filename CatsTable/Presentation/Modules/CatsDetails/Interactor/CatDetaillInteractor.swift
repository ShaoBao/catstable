//
//  CatDetaillInteractor.swift
//  CatsTable
//
//  Created by Gleb on 28/04/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation


protocol  CatDetailInteractor: class {
    func getId(id: String, imageURLString: String, catJSON: AnyObject)
    func getCatById()
}
