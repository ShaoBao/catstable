//
//  CatDetailInteractorImpl.swift
//  CatsTable
//
//  Created by Gleb on 28/04/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation
import  UIKit

class CatDetailInteractorImpl {
    
    
    private let catService: CatsService
    private let presenter: CatDetailsPresenter
    weak var viewController: CatsDetailViewControllerImpl?
    
    var catId: String?
    var catJSON: AnyObject?
    
    init(catService: CatsService, presenter: CatDetailsPresenter) {
        self.catService = catService
        self.presenter = presenter
    }
    
    func fetchImageAndJSON(url: String, catJSON: AnyObject) {
        var data: Data?
        let url = URL(string: url)
        let dispatchQueue = DispatchQueue.global(qos: .userInitiated)
        
        let worker = DispatchWorkItem.init(qos: .userInitiated, flags: .barrier) {
            if let url = url {
                data =  try? Data(contentsOf: url)
            }
        }
        dispatchQueue.async(execute: worker)
        worker.notify(queue: .main) {
            if let data = data {
                self.presenter.presentImageAndData(image: data, catJSON: catJSON)
            }
        }
    }
}

extension CatDetailInteractorImpl: CatDetailInteractor {
    
    
    func getCatById() {
        self.catService.requestCatById(id: self.catId!) { (cats, error) in
            
        }
    }
    
    func getId(id: String,imageURLString: String, catJSON: AnyObject) {
        self.catId = id
        self.catJSON = catJSON
        
        fetchImageAndJSON(url: imageURLString, catJSON: catJSON)
    }
}
