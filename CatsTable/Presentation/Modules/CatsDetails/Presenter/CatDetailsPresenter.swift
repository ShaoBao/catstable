//
//  CatDetailsPresenter.swift
//  CatsTable
//
//  Created by Gleb on 28/04/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation



protocol CatDetailsPresenter: class {
    func showDetails()
    func presentImageAndData(image: Data, catJSON: AnyObject)
}
