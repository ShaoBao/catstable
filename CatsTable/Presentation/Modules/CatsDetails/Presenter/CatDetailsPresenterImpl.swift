//
//  CatDetailsPresenterImpl.swift
//  CatsTable
//
//  Created by Gleb on 28/04/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation


class CatDetailsPresenterImpl {
    
    var viewController: CatsDetailViewControllerImpl
    private let router: CatDetailRouter
    
    init(viewController: CatsDetailViewControllerImpl, router: CatDetailRouter) {
        self.viewController = viewController
        self.router = router
    }
}

extension CatDetailsPresenterImpl: CatDetailsPresenter {
    func presentImageAndData(image: Data, catJSON: AnyObject) {
        viewController.displayImageAndData(image: image, catJSON: catJSON)
    }
    

    func showDetails() {
        //MARK waiting for future needs
    }
 
}
